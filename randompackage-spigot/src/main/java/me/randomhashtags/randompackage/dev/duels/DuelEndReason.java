package me.randomhashtags.randompackage.dev.duels;

public enum DuelEndReason {
    PLAYER_LEFT_CMD,
    PLAYER_LEFT_QUIT,
    CHOOSE_WINNER
}
