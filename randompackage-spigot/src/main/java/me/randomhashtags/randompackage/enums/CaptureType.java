package me.randomhashtags.randompackage.enums;

public enum CaptureType {
    CIRCLE,
    SQUARE,
}
