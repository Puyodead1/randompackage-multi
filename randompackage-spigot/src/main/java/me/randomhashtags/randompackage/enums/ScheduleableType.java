package me.randomhashtags.randompackage.enums;

public enum ScheduleableType {DAILY, MONTHLY, WEEKLY}
